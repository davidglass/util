﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Web.Script.Serialization; // in System.Web.Extensions

namespace DbUtil
{
    public class ActiveRecord
    {
        public enum EntityType { Proc, Table, View }
        public int Id { get; set; }  // trying to move this
        [ScriptIgnore]
        public SmartDbContext Context { get; set; }
        [ScriptIgnore]
        public string Source { get; set; } // can be Proc, Table, or View
        [ScriptIgnore]
        public EntityType SourceType { get; set; }
        [ScriptIgnore]
        public object RecordParams { get; set; }

        //public ActiveRecord()
        //{
        //}

        // **NOTE**, this must be parameterless constructor for SmartProc to work properly...
        ////public ActiveRecord(System.Data.Entity.DbContext ctx, source, EntityType sourceType, object RecordParams) {
        //// ctor:
        //public ActiveRecord(SmartDbContext ctx, string src, EntityType stype, object rp)
        //{
        //    Context = ctx;
        //    Source = src;
        //    SourceType = stype;
        //    RecordParams = rp;
        //}

        public T Get<T>()
        {
            switch (SourceType)
            {
                case EntityType.Proc:
                    {
                        var mi = typeof(SmartDbContext).GetMethod("GetSingleByProc");
                        //var mytype = this.GetType();
                        //var getOne = mi.MakeGenericMethod(mytype);
                        var getOne = mi.MakeGenericMethod(typeof(T));
                        object[] args = new object[2];
                        args[0] = Source;
                        args[1] = RecordParams;
                        var result = getOne.Invoke(Context, args);
                        //return result;
                        return (T)Convert.ChangeType(result, typeof(T));
                    }
                default:
                    {
                        return default(T);
                        // can't return null on generic method...;
                    }
            }
        }

        public List<Dictionary<string, object>> GetAll()
        {
            var et = this.SourceType;
            var ctx = this.Context;
            var sourceName = this.Source;
            var rp = this.RecordParams;

            var result = new List<Dictionary<string,object>>();
            if (et == EntityType.Proc)
            {
                result = ctx.GetByProc(sourceName, rp);
            }
            else if (et == EntityType.Table || et == EntityType.View)
            {
                // TODO: build query on View or Table from rp object
                throw new NotImplementedException("Under Construction...");
            }
            return result;
        }
        //public static List<T> GetAll<T>(SmartDbContext ctx, EntityType et, string sourceName, object rp)
        public List<T> GetAll<T>()
        {
            var et = this.SourceType;
            var ctx = this.Context;
            var sourceName = this.Source;
            var rp = this.RecordParams;

            object result = null;
            if (et == EntityType.Proc)
            {
                // TODO: non-generic SmartDbContext.GetByProc returning List<Dictionary<string,object>>
//                var resulttype = typeof(T);
                // if resulttype is Dictionary<string,object>, GetByProc should
                // simply convert from DataTable / DataRow to string-keyed columns, rather than enumerating properties...
                //var resulttype = typeof(T);
                // assuming Proc for now...later may add FK-joins / EF automation / etc.
                var mi = typeof(SmartDbContext).GetMethod("GetByProc");
                //            var mytype = this.GetType();
                var getAll = mi.MakeGenericMethod(typeof(T));
                //var procParams = new { Id = Id };
                object[] args = new object[2];
                args[0] = sourceName;
                args[1] = rp;
                //            var result = getAll.Invoke(this.Context, args);
                result = getAll.Invoke(ctx, args);
            }
            else if (et == EntityType.Table || et == EntityType.View)
            {
                // TODO: build query on View or Table from rp object
                throw new NotImplementedException("Under Construction...");
            }
            return (List<T>)Convert.ChangeType(result, typeof(List<T>));
        }
    }
}