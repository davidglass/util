﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;

namespace DbUtil
{
    public class SmartDbContext : System.Data.Entity.DbContext
    {
        public SmartDbContext(string namedConnStr)
            : base(namedConnStr)
        {
        }

        public bool Exec(string procName, dynamic parameters)
        {
            return new SmartProc(procName, Database.Connection.ConnectionString, parameters).Exec();
        }

        public List<T> GetByProc<T>(string procName, dynamic parameters)
        {
            var sp = new SmartProc(procName, Database.Connection.ConnectionString, parameters);
            List<T> result = sp.All<T>();
            return result;
        }

        public List<Dictionary<string,object>> GetByProc(string procName, dynamic parameters)
        {
            var sp = new SmartProc(procName, Database.Connection.ConnectionString, parameters);
            return sp.All();
        }

        public T GetSingleByProc<T>(string procName, dynamic parameters)
        {
            var r = GetByProc<T>(procName, parameters);
            if (r.Count != 1)
            {
                // TODO: Logging util
                throw new Exception("non-single result");
            }
            return r[0];
        }
    }
}
